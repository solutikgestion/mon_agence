<?php

namespace App\Form;

use App\Entity\PropertyOption;
use App\Entity\PropertySearch;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertySearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('minSurface', IntegerType::class, [
                'required' => false,
                'label' => 'Surface minimum',
                'attr' => [
                    'placeholder' => 'Surface minimum',
                ]
            ])
            ->add('maxPrice', IntegerType::class, [
                'required' => false,
                'label' => 'Budget maximum',
                'attr' => [
                    'placeholder' => 'Budget maximum',
                ]
            ])
            ->add('options', EntityType::class, [
                'class' => PropertyOption::class,
                'choice_label' => 'name', /* entity field */
                'multiple' => true,
                'required' =>false,
                'label' => 'Options',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PropertySearch::class,
            'method' => 'GET', //elements send in get method
            'csrf_protection' => false, //no need token for the search form (no action in db)
        ]);
    }

    //to format url on get method when form is submit,
    // change prefixe default value by empty value
    //ie : property_search[maxPrice] will become only maxPrice
    public function getBlockPrefix()
    {
        return '';
    }
}
