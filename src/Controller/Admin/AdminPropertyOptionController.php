<?php

namespace App\Controller\Admin;

use App\Entity\PropertyOption;
use App\Form\PropertyOptionType;
use App\Repository\PropertyOptionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/option")
 */
class AdminPropertyOptionController extends AbstractController
{
    /**
     * @Route("/", name="admin.propertyOption.index", methods={"GET"})
     * @return Response
     */
    public function index(PropertyOptionRepository $propertyOptionRepository): Response
    {
        return $this->render('admin/propertyOption/index.html.twig', [
            'propertyOptions' => $propertyOptionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin.propertyOption.new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $option = new PropertyOption();
        $form = $this->createForm(PropertyOptionType::class, $option);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($option);
            $entityManager->flush();

            $this->addFlash('success', 'Option enregistrée !');
            return $this->redirectToRoute('admin.propertyOption.index');
        }

        return $this->render('admin/propertyOption/new.html.twig', [
            'option' => $option,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="admin.propertyOption.edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PropertyOption $option): Response
    {
        $form = $this->createForm(PropertyOptionType::class, $option);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Option modifiée !');

            return $this->redirectToRoute('admin.propertyOption.index', [
                'id' => $option->getId(),
            ]);
        }

        return $this->render('admin/propertyOption/edit.html.twig', [
            'option' => $option,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin.propertyOption.delete", methods={"DELETE"})
     */
    public function delete(Request $request, PropertyOption $option): Response
    {
        if ($this->isCsrfTokenValid('delete'.$option->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($option);
            $entityManager->flush();
        }

        $this->addFlash('success', 'Option supprimée !');
        return $this->redirectToRoute('admin.propertyOption.index');
    }
}
