<?php
/**
 * Created by PhpStorm.
 * User: rom1
 * Date: 16/01/19
 * Time: 22:37
 */

namespace App\Controller;


use App\Entity\Property;
use App\Entity\PropertySearch;
use App\Form\PropertySearchType;
use App\Repository\PropertyRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PropertyController extends AbstractController
{
    /**
     * @var PropertyRepository
     */
    private $repository;

    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(PropertyRepository $repository, ObjectManager $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/biens", name="property.index", methods={"GET"})
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $search = new PropertySearch();

        $form = $this->createForm(PropertySearchType::class, $search);
        $form->handleRequest($request);

        $properties = $paginator->paginate(
            $this->repository->findAllVisibleQuery($search), /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            12/*limit per page*/
        );

        return $this->render('property/index.html.twig', [
            'current_menu'  => 'properties',
            'properties'    => $properties,
            'searchForm'    => $form->createView(),
        ]);
    }

    /**
     * @Route("/biens/{slug}-{id}", name="property.show", requirements={"slug": "[a-z0-9\-]*", "id": "\d+"})
     * @param Property $property
     * @return Response
     * @ParamConverter()
     */
    public function show(Property $property, string $slug): Response
    {
        //Avoid to instanciate several times new Slugify called in $property->getSlug
        //Because it is used in if condition and in slug parameter in redirection
        $slugReference = $property->getSlug();

        //Test slug refers to property title is identical with slug contain in the link on the page
        //(prevent any malicious modification of link in html page)
        if ($slugReference !== $slug) {
            return $this->redirectToRoute('property.show',[
                'id' => $property->getId(),
                'slug' => $slugReference,
            ],301);
        }

        return $this->render('property/show.html.twig', [
            'current_menu' => 'properties',
            'property' => $property,
        ]);

    }

}